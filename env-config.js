const CLIENT_ID = "K79n3NBhdwE2EB606mL1YjnIgGh4k6p1RvDv_sxPHpI";

const apiUrls = {
  lazyImg: `https://api.unsplash.com/photos?client_id=${CLIENT_ID}`,
  listTeams: "http://private-12729-proyect1.apiary-mock.com/list-teams"
};

module.exports = {
  apiUrls
};
