const mainValidator = require("validators/main");

const mainResponseData = {
  credenciales: {
    user: "edflores004",
    password: "012345"
  },
  header: {
    logoImg: "",
    logoURL: "",
    newWindowLogo: "",
    nombre: "",
    bannerImg: "",
    bannerUrl: "",
    newWindowBanner: "",
    tituloBienvenida: "",
    tituloFontSize: "",
    tituloColor: ""
  },
  body: [
    {
      idTeam: "",
      nombreTeam: "",
      teamFontSize: "",
      teamColor: "",
      players: [
        {
          playerid: "",
          playerImg: "",
          playerImgUrl: "",
          playerNombre: "",
          playerApellido: "",
          playerEdad: "",
          playerPasatiempo: "",
          playerWebSite: "",
          playerProfesion: ""
        }
      ]
    }
  ]
};

describe("Probando el validator", () => {
  it("Probando que el JSON recibido sea correcto y el validator nos devuelva true", () => {
    const validateMainData = mainValidator.validateMainData;
    const response = validateMainData(mainResponseData);
    expect(response).not.toBeNull();
    expect(response).not.toBeUndefined();
  });
});
