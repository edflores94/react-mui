import { validate } from "validators/base";
import MainModel from "models/mainModel";

export function validateMainData(data) {
  return validate(MainModel, data);
}
