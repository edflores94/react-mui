import getMainApi from "api/main";
import { getMainRequest } from "requests/requests";

async function infoLoadProccess(successCallback) {
  try {
    const mainRequest = getMainRequest();
    getMainApi(mainRequest).then((response) => {
      successCallback(response.data);
    });
  } catch (error) {
    console.log(error);
  }
}

export default infoLoadProccess;
