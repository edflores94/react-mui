// /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
export const minLenghtReg = /^[0-9A-Za-z\d@#$%&*=]{8,}$/;
export const minusMayusReg = /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z\d@$!%*?&].*$/;
export const specialCharReg = /^(?=.*[#$%&*=])[A-Za-z\d#$%&*=].*$/;
