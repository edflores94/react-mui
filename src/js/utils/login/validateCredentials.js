import { minLenghtReg, minusMayusReg, specialCharReg } from "./regex";
//some regex expressions

export function validateMinLenght(value) {
  const regex = minLenghtReg;
  return regex.test(value);
}

export function validateMinusMayus(value) {
  const regex = minusMayusReg;
  return regex.test(value);
}

export function validateSpecialChar(value) {
  const regex = specialCharReg;
  return regex.test(value);
}
