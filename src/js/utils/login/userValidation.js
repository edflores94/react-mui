import { validateMinLenght, validateMinusMayus, validateSpecialChar } from "./validateCredentials";
import { CREDENTIAL_ERRORS } from "common/constants";

export function isUsernameValid(username) {
  if (username !== "" && username != undefined && username != null) {
    return true;
  }
  return false;
}

export function getCodeErrors(payload) {
  const { user, password } = payload;
  let errors = [];
  //validating mininum lenght of password
  const minLenghValidation = validateMinLenght(password);
  if (!minLenghValidation) {
    errors.push(CREDENTIAL_ERRORS[0].value);
  }

  //validating that contain at least 1 mayus and 1 minus char
  const minMayusValidation = validateMinusMayus(password);
  if (!minMayusValidation) {
    errors.push(CREDENTIAL_ERRORS[1].value);
  }

  //validating that contain at least 1 special char
  const specialCharValidation = validateSpecialChar(password);
  if (!specialCharValidation) {
    errors.push(CREDENTIAL_ERRORS[2].value);
  }
  return errors;
}
