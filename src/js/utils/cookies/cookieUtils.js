import Cookies from "universal-cookie";

export default class CookiesUtils {
  static getCookie(name) {
    const cookies = new Cookies();
    return cookies.get(name) !== undefined || cookies.get(name) !== null ? cookies.get(name) : false;
  }

  static setCookie(name, value, options = {}) {
    const cookies = new Cookies();
    cookies.set(name, value, options);
  }

  static removeCookie(name) {
    const cookies = new Cookies();

    if (cookies.get(name) != undefined) {
      cookies.remove(name, { path: "/" });
      return true;
    } else {
      return false;
    }
  }
}
