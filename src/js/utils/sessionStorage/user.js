import { saveItem, getItem, removeItem } from "./base";

const USER_NAME = "username";

export function saveUserNameSS(value) {
  const saved = saveItem(USER_NAME, value);
  return saved;
}

export function getUserNameSS() {
  return getItem(USER_NAME);
}

export function removeUserNameSS() {
  removeItem(USER_NAME);
}
