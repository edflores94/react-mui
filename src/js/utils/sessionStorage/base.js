export function saveItem(name, value) {
  // Save an item value in sessionStorage
  if (typeof Storage !== "undefined") {
    sessionStorage.setItem(name, value);
  }
}

export function getItem(name) {
  // Get an item value in sessionStorage
  if (typeof Storage !== "undefined") {
    return sessionStorage.getItem(name);
  }
  return undefined;
}

export function removeItem(name) {
  // Remove an item value in sessionStorage
  if (typeof Storage !== "undefined") {
    sessionStorage.removeItem(name);
  }
}
