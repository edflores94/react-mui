import React from "react";
import { Box, Paper, Typography } from "@mui/material";
import PropTypes from "prop-types";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import Carousel from "react-material-ui-carousel";
import styles from "./NormalCarousel.scss";

NormalCarousel.propTypes = {
  images: PropTypes.array
};
function NormalCarousel(props) {
  const { images } = props;
  return (
    <div className={styles.wrapper} style={{ marginTop: "50px", color: "#494949" }}>
      <Carousel
        animation="fade"
        navButtonsAlwaysVisible
        autoPlay
        interval={3000}
        PrevIcon={<SkipPreviousIcon />}
        NextIcon={<SkipNextIcon />}
      >
        {images.map((item, i) => (
          <Paper key={`test3-item-${i}`} elevation={10} style={{ height: "auto" }} className={styles.heightItem}>
            <Box
              component="img"
              sx={{
                height: "auto",
                display: "block",
                maxWidth: 800,
                overflow: "hidden",
                width: "100%"
              }}
              src={item.imgPath}
              alt={item.label}
            />
          </Paper>
        ))}
      </Carousel>

      <br />
      <Typography style={{ marginTop: 20 }}>
        Note: The carousel doesn't change height as the input values change, but only when the active child changes.
      </Typography>
    </div>
  );
}

export default NormalCarousel;
