import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "views/Home";
import Main from "views/Main";
import { routeCodes, publicPath } from "common/routeConfig";

function Routes() {
  return (
    <Switch>
      <Route exact path={publicPath} component={Home} />
      <Route path={routeCodes.MAIN} component={Main} />
    </Switch>
  );
}

export default Routes;
