const MainModel = {
  credenciales: {
    type: "object",
    optional: false,
    properties: {
      user: {
        type: "string",
        optional: true
      },
      password: {
        type: "string",
        optional: true
      }
    }
  },
  header: {
    type: "object",
    optional: false,
    properties: {
      logoImg: {
        type: "string",
        optional: true
      },
      logoURL: {
        type: "string",
        optional: true
      },
      newWindowLogo: {
        type: "boolean",
        optional: false
      },
      nombre: {
        type: "string",
        optional: true
      },
      bannerImg: {
        type: "string",
        optional: true
      },
      bannerUrl: {
        type: "string",
        optional: true
      },
      newWindowBanner: {
        type: "boolean",
        optional: false
      },
      tituloBienvenida: {
        type: "string",
        optional: true
      },
      tituloFontSize: {
        type: "string",
        optional: true
      },
      tituloColor: {
        type: "string",
        optional: true
      }
    }
  },
  body: {
    type: "object",
    optional: false,
    isArray: true,
    item: {
      type: "object",
      optional: false,
      properties: {
        idTeam: {
          type: "string",
          optional: false
        },
        nombreTeam: {
          type: "string",
          optional: true
        },
        teamFontSize: {
          type: "string",
          optional: true
        },
        teamColor: {
          type: "string",
          optional: true
        },
        players: {
          type: "object",
          optional: false,
          isArray: true,
          item: {
            type: "object",
            optional: false,
            properties: {
              playerid: {
                type: "string",
                optional: true
              },
              playerImg: {
                type: "string",
                optional: true
              },

              playerImgUrl: {
                type: "string",
                optional: true
              },
              playerNombre: {
                type: "string",
                optional: true
              },
              playerApellido: {
                type: "string",
                optional: true
              },
              playerEdad: {
                type: "string",
                optional: true
              },
              playerPasatiempo: {
                type: "string",
                optional: true
              },
              playerWebSite: {
                type: "string",
                optional: true
              },
              playerProfesion: {
                type: "string",
                optional: true
              }
            }
          }
        }
      }
    }
  }
};

export default MainModel;
