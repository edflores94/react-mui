import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "@babel/polyfill";
import App from "views/App";
import Font from "common/fontImports";
import "scss/main.scss";

const RootComponent = () => (
  <BrowserRouter>
    <Font />
    <App />
  </BrowserRouter>
);

ReactDOM.render(<RootComponent />, document.getElementById("root"));
