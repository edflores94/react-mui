import { API_URLS } from "common/apiUrls";

export function getMainRequest() {
  return {
    method: "GET",
    headers: {
      Accept: "application/json",
      ["Content-Type"]: "application/json"
    },
    url: API_URLS.listTeams
  };
}
