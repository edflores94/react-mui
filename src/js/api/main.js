//import { validateMainData } from "validators/main";
import { makeAxiosRequest } from "utils/requests/webRequest";

export default async function getMainApi(request) {
  const response = {
    success: false,
    statusCode: 0,
    data: null
  };

  try {
    const webResponse = await makeAxiosRequest(request);

    const { status: statusCode } = webResponse;

    response.statusCode = statusCode;

    if (statusCode === 200) {
      const { data } = webResponse;
      //const validData = validateMainData(data);

      if (data) {
      //if (validData) {
        response.success = true;
        response.data = data;

        return Promise.resolve(response);
      }
    }
  } catch (ex) {
    debugger;
  }

  return Promise.reject(response);
}
