export const CREDENTIAL_ERRORS = [
  { value: "Error: longitud minima de 8 caracteres" },
  { value: "Error: debe tener al menos 1 letra mayúscula y 1 minúscula" },
  { value: "Error: debe tener al menos un caracter especial (#$%&*=)" }
];

export const USER_INVALID_MSG = "Usuario Invalido";
