export const publicPath = "/";

export const routeCodes = {
  HOME: `${publicPath}`,
  LOGGED_IN: `${publicPath}home`,
  MAIN: `${publicPath}main`
};
