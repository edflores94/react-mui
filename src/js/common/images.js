const imagesUrl = {
  BREAKFAST: "https://mui.com/static/images/buttons/breakfast.jpg",
  BURGERS: "https://mui.com/static/images/buttons/burgers.jpg",
  CAMERA: "https://mui.com/static/images/buttons/camera.jpg"
};
export default imagesUrl;
