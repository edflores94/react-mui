import { hot } from 'react-hot-loader/root';
import React from "react";
import PropTypes from "prop-types";
import Routes from "components/Routes/Routes";

App.propTypes = {
  location: PropTypes.object
};

function App() {
  function render() {
    return <Routes />;
  }

  return render();
}

export default hot(App);
