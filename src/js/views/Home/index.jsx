import React, { useState, useEffect } from "react";
import infoLoadProccess from "logic/mainCall";
//mui components
import { CssBaseline } from "@mui/material";
//components
import SwipeableCarousel from "components/Carousel/SwipeableCarousel/SwipeableCarousel";
//utils
import { getCodeErrors, isUsernameValid } from "utils/login/userValidation";
import { USER_INVALID_MSG } from "common/constants";
import { images } from "common/data";
import styles from "./Home.scss";

function Home(props) {
  const [renderFlag, setRenderFlag] = useState(false);
  const [isUserError, setIsUserError] = useState(false);
  const [userError, setUserError] = useState("");
  const [isPasswordError, setIsPasswordError] = useState(false);
  const [errorTexts, setErrorTexts] = useState([]);
  const [data, setData] = useState("");

  useEffect(() => {
    infoLoadProccess(handleInfoSuccess);
  }, []);

  function handleInfoSuccess(response) {
    const credentials = response.credenciales;
    const userValid = isUsernameValid(credentials.user);
    if (!userValid) {
      setIsUserError(true);
      setUserError(USER_INVALID_MSG);
    } else {
      const errors = getCodeErrors(credentials);
      if (errors.length > 0) {
        setIsPasswordError(true);
        setErrorTexts(errors);
      } else {
        setRenderFlag(true);
        setData(response);
      }
    }
  }

  /** render functions */
  if (renderFlag) {
    const header = data?.header;

    return (
      <CssBaseline>
        <div className={styles.header}>
          <div className={styles.user}>{data?.credenciales?.user}</div>
          <br />
          <div className={styles.logo}>
            <img src={header?.logoImg} alt="img" />
          </div>
        </div>

        <div className={styles.carouselDiv}>
          <SwipeableCarousel images={images} />
        </div>
      </CssBaseline>
    );
  } else {
    return (
      <div>
        {isUserError && <div> {userError}</div>}
        {isPasswordError && errorTexts && errorTexts.map((item) => <p>{item}</p>)}
      </div>
    );
  }
}

export default Home;
