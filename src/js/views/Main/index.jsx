import React, { useState, useEffect } from "react";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
//mui components
import { Box, Button, Container, Paper, Typography } from "@mui/material";
//custom components
import Normal from "components/Form/Toggle/Normal";
import styles from "./Main.scss";

function Main() {
  const [renderFlag, setRenderFlag] = useState(true);
  const [open, setOpen] = useState(false);

  useEffect(async () => {
    setRenderFlag(true);
  }, []);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const action = (
    <div>
      <Button color="secondary" size="small" onClick={handleClose}>
        UNDO
      </Button>
      <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </div>
  );

  const serviceList = ["Service1", "Service2", "Service3"];

  if (renderFlag) {
    return (
      <Container>
        <Typography variant="h1" sx={{ my: 4, textAlign: "center", color: "primary.main" }}>
          Services
        </Typography>
        <Typography variant="h2">Overview</Typography>
        <Box
          sx={{
            display: "flex",
            flexDirection: { xs: "column", md: "row" },
            justifyContent: "space-between",
            pt: 4,
            gap: 4
          }}>
          {serviceList.map((service) => {
            return (
              <Paper elevation={3} sx={{ width: { xs: 1, md: 320 } }}>
                <Box sx={{ m: 3 }}>
                  <Typography variant="h3">{service}</Typography>
                  <Typography>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur corrupti aperiam
                    corporis unde quasi odio perferendis est nemo veritatis sit aliquam, voluptatem aliquid
                    rem, dignissimos officiis. Rerum ipsam reprehenderit architecto.
                  </Typography>
                  <Button variant="contained" color="secondary" sx={{ mt: 2 }}>
                    Learn more
                  </Button>
                </Box>
              </Paper>
            );
          })}
        </Box>

        <div className={styles.mainWrapper}>
          <div className={styles.left}>
            <Button onClick={handleClick}>Open simple snackbar</Button>
            <Snackbar
              open={open}
              autoHideDuration={6000}
              onClose={handleClose}
              message="Note archived"
              action={action}
            />
          </div>
          <div className={styles.right}>
            <Normal />
          </div>
        </div>
      </Container>
    );
  }
  return null;
}

export default Main;
