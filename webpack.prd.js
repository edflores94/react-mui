const path = require("path");
const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const jsSourcePath = path.resolve(__dirname, "src/js");
const generalStylePath = path.resolve(__dirname, "src/scss");

const getStyleLoaders = shouldUseCSSModules => {
  return [
    {
      loader: MiniCssExtractPlugin.loader,
      //options: {
        //hmr: false
      //}
    },
    !shouldUseCSSModules
      ? "css-loader"
      : "css-loader?module&importLoaders=1&localIdentName=[name]_[local]",
    "sass-loader"
  ];
};

const config = {
  mode: "production",
  module: {
    rules: [
      {
        test: /\.scss$/,
        include: [generalStylePath],
        exclude: ["/node_modules/"],
        use: getStyleLoaders(false)
      },
      {
        test: /\.scss$/,
        include: [jsSourcePath],
        exclude: ["/node_modules/"],
        use: getStyleLoaders(true)
      }
    ]
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        test: /\.js(\?.*)?$/i,
        parallel: true,
        //sourceMap: false,
        terserOptions: {
          ecma: undefined,
          warnings: false,
          parse: {},
          compress: {
            arrows: true,
            booleans: true,
            collapse_vars: true,
            comparisons: true,
            computed_props: true,
            conditionals: true,
            dead_code: true,
            defaults: true,
            directives: true,
            drop_console: true,
            drop_debugger: true,
            if_return: true,
            inline: true,
            join_vars: true,
            loops: true,
            passes: 3,
            properties: true,
            sequences: true,
            switches: true,
            unsafe_proto: true,
            warnings: false
          },
          mangle: true, // Note `mangle.properties` is `false` by default.
          module: false,
          output: {
            comments: false
          },
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_classnames: undefined,
          keep_fnames: false,
          safari10: false
        }
      })
    ]
  }
};

module.exports = merge(common, config);
